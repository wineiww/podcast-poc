import React from 'react';
import {
  Button,
  Image,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';
import TrackPlayer from 'react-native-track-player';

const track = {
  id: 'hello',
  url: 'http://aod.streamguys.com/atimemedia-live/playlist.m3u8',
  title: 'Podcast',
  artist: 'GMM',
  type: 'hls',
  artwork:
    'https://pbs.twimg.com/profile_images/925947000696160257/ckTlaFG8.jpg',
};

const App = () => {
  React.useEffect(() => {
    TrackPlayer.add([track]);
    TrackPlayer.updateOptions({
      stopWithApp: false,
      capabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
        TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
      ],
      compactCapabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
        TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
      ],
    });
  }, []);

  const handlePlay = async () => {
    TrackPlayer.play();
  };
  const handlePause = async () => {
    TrackPlayer.pause();
  };

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <View style={styles.body}>
          <Image style={styles.image} source={{uri: track.artwork}} />
          <View style={styles.actions}>
            <Button title="Play" onPress={handlePlay} />
            <Button title="Pause" onPress={handlePause} />
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  body: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  actions: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  image: {
    width: 200,
    height: 200,
    marginBottom: 20,
  },
});

export default App;
